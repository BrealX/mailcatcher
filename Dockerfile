FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
WORKDIR /srv

RUN apk add --no-cache \
    bash \
    gcc \
    build-base

RUN pip install mailtrap

EXPOSE 1080 1025

CMD ["mailtrap", "--foreground", "--db=./mails.sqlite", "--smtp-ip=0.0.0.0", "--http-ip=0.0.0.0"]
