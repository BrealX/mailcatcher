# We Meet - Mailcatcher

### Run

To run the service you need to build the image from the Dockerfile and start the container.
The service can be added to the `docker-compose.yaml` file of the CI.
When added the message engine starts to send all emails to the mailcatcher's 1025 port.
All the sent messages then are accessible in the browser at `http://localhost:1080`
